import axios from "axios";
import jwt_decode from 'jwt-decode'
import {InvalidRefreshToken, NotAuthenticatedError, TokenExpiredError} from "@/auth/errors";

export async function isAuth(): Promise<boolean> {
  try {
    if (await verifyToken(localStorage.access)) {
      return true
    } else {
      localStorage.removeItem('access')
      localStorage.removeItem('refresh')
      return false
    }
  } catch (e) {
    if (e instanceof TokenExpiredError) {
      try {
        localStorage.access = await refreshToken(localStorage.refresh)
        return true
      } catch (e) {
        if (e instanceof TokenExpiredError || e instanceof InvalidRefreshToken) {
          localStorage.removeItem('access')
          localStorage.removeItem('refresh')
          return false
        }
        throw e
      }
    }
    throw e
  }
}

async function verifyToken(rawToken: any): Promise<boolean> {
  if (!rawToken) {
    return false
  }
  let token = (jwt_decode(rawToken) as any)
  if (token.exp < Date.now() / 1000) {
    throw new TokenExpiredError()
  }

  const options = {
    method: 'POST',
    url: import.meta.env.VITE_API_URL + '/api/token/verify/',
    headers: {'Content-Type': 'application/json'},
    data: {token: rawToken}
  }

  try {
    await axios.request(options)
    return true
  } catch (e: any) {
    if (e.response.status === 401) {
      return false
    }
    throw e
  }
}

async function refreshToken(rawToken: any): Promise<string> {
  if (!rawToken) {
    throw new InvalidRefreshToken()
  }
  let token = (jwt_decode(rawToken) as any)
  if (token.exp < Date.now() / 1000) {
    throw new TokenExpiredError()
  }

  const options = {
    method: 'POST',
    url: import.meta.env.VITE_API_URL + '/api/token/refresh/',
    headers: {'Content-Type': 'application/json'},
    data: {refresh: rawToken}
  }

  try {
    let response = await axios.request(options)
    return response.data.access
  } catch (e: any) {
    if (e.response.status === 401) {
      throw new InvalidRefreshToken()
    }
    throw e
  }
}

export function getUserId(): number {
  let id = +(jwt_decode(localStorage.access) as any).user_id
  if (id) {
    return id
  } else {
    throw NotAuthenticatedError
  }
}
