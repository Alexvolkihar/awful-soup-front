import BaseConnector from './baseConnector'
import Ingredient from "@/models/ingredient";
import {arrayField, enumField, numberField, stringField} from "@/models/fields";
import {isAuth} from "@/auth/auth";
import {NotAuthenticatedError} from "@/auth/errors";
import {GraphQLMutationError, GraphQLQueryError} from "@/api/errors";

export default class IngredientConnector extends BaseConnector<Ingredient> {
  async retrieve(query: string): Promise<Ingredient> {
    const result = await this.axiosCall({
      method: "post",
      data: {
        query: query
      },
      headers: {
        'Content-Type': 'application/json'
      }
    })
    if (result.data.ingredient) {
      const ingredient = new Ingredient()
      ingredient.fromApi(result.data.ingredient)
      return ingredient;
    } else {
      return new Ingredient()
    }
  }

  async list(query: any): Promise<Array<Ingredient>> {
    const result = await this.axiosCall({
      method: "post",
      data: {
        query: query
      },
      headers: {
        'Content-Type': 'application/json'
      }
    })
    return result.data.allIngredients?.edges.map((raw_ingredient: any) => {
      const ingredient = new Ingredient()
      ingredient.fromApi(raw_ingredient.node)
      return ingredient
    });

  }

  async mutate(data: Ingredient): Promise<number> {
    if (!(await isAuth())) {
      throw new NotAuthenticatedError()
    }
    const result = await this.axiosCall({
      method: "post",
      data: {
        query: `mutation {
            ingredientMutation(input: {
                ${numberField(data.getId(), 'id')}
                ${stringField(data.name, 'name')}
                ${enumField(data.ingredient_type, 'ingredientType')}
                ${arrayField(data.tags?.map((tag => tag.getId())), 'tags')}
                ${stringField(String(data.alt?.getId()), 'alt')}
                ${stringField(data.definition, 'definition')}
                ${stringField(data.examples, 'examples')}
                ${arrayField(data.bases?.map((base => base.getId())), 'bases')}
            }){
                errors{messages, field}
                id
            }
        }`
      },
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.access
      }
    })
    if (result.data?.ingredientMutation?.errors) {
      throw new GraphQLMutationError(result.data?.ingredientMutation?.errors)
    } else if (result.errors) {
      throw new GraphQLQueryError(result.data?.ingredientMutation?.errors)
    }
    return +result.data.ingredientMutation.id
  }
}

export const INGREDIENT_LIST_FIELDS = `
    id
    name
    alts{
      edges {
        node {
          id
          name
        }
      }
    }
    definition
    examples
    tags{
      edges {
        node {
         id
         name
         color
        }
      }
    }
    createdBy {
        username
    }`

export const INGREDIENT_DETAILS_FIELDS = `
createdBy {
            username
        }
        id
        name
        alts{
          edges {
            node {
              id
              name
            }
          }
        }
        definition
        examples
        tags{
          edges {
            node {
             id
             name
             color
            }
          }
        }
    }`
