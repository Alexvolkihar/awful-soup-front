import BaseConnector from "@/api/baseConnector";
import {User} from "@/models/user";
import Ingredient from "@/models/ingredient";
import {NotImplementedError} from "@/models/errors";

export class UserConnector extends BaseConnector<User> {
  async retrieve(query: string): Promise<User> {
    const result = await this.axiosCall({
      method: "post",
      data: {
        query: query
      },
      headers: {
        'Content-Type': 'application/json'
      }
    })
    if (result.data.user) {
      const user = new User()
      user.fromApi(result.data.user)
      return user;
    } else {
      return new User()
    }
  }

  async list(query: any): Promise<Array<User>> {
    const result = await this.axiosCall({
      method: "post",
      data: {
        query: query
      },
      headers: {
        'Content-Type': 'application/json'
      }
    })
    return result.data.allUsers?.edges.map((raw_user: any) => {
      const user = new Ingredient()
      user.fromApi(raw_user.node)
      return user
    });

  }

  mutate(data: User): Promise<number> {
    throw NotImplementedError
  }
}
