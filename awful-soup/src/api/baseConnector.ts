import axios, {AxiosInstance, AxiosRequestConfig} from "axios";
import BaseModel from "@/models/baseModel";

export default abstract class BaseConnector<T extends BaseModel> {
  private axiosInstance: AxiosInstance

  constructor() {
    this.axiosInstance = axios.create({
      baseURL: import.meta.env.VITE_API_URL + `/graphql`
    })
  }

  protected async axiosCall(config: AxiosRequestConfig) {
    try {
      const {data} = await this.axiosInstance.request(config)
      return data
    } catch (error) {
      throw error
    }
  }

  abstract retrieve(query: string): Promise<T>

  abstract list(query: string): Promise<Array<T>>

  abstract mutate(data: T): Promise<number>
}
