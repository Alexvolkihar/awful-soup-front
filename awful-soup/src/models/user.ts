import BaseModel from "@/models/baseModel";
import Ingredient from "@/models/ingredient";

export enum NodeType {
  UserNode = "UserNode"
}

export class User extends BaseModel {
  username?: string
  first_name?: string
  last_name?: string
  isActive?: boolean
  is_staff?: boolean
  is_superuser?: boolean
  ingredient_set?: Ingredient[]

  fromApi(node: any) {
    this.id = node.id
    this.username = node.username
    this.first_name = node.firstName
    this.last_name = node.lastName
    this.is_staff = node.isStaff
    this.is_superuser = node.isSuperuser

    this.ingredient_set = node.ingredientSet?.edges.map((raw_ingredient: any) => {
      const ingredient = new Ingredient()
      ingredient.fromApi(raw_ingredient.node)
      return ingredient
    })
  }

  static encodeId(db_id: number, nodeType: NodeType = NodeType.UserNode): string {
    return btoa(nodeType + ':' + db_id)
  }
}
